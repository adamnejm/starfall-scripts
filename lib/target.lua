--@name Player Target Library
--@author Name
--@server

local function match(ply, identifier)
	return
		ply == identifier or
		type(identifier) == "string" and
		(ply:getSteamID() == identifier or
		ply:getSteamID64() == identifier or
		string.find(string.lower(ply:getName()), string.lower(identifier), 1, true))
end

--- Matches all players against a whitelist and blacklist
---@param whitelist? table|Entity|string|true Name of the player, their SteamID, player entity or an array of those. True or `nil` to match all players
---@param blacklist? table|Entity|string|true Name of the player, their SteamID, player entity or an array of those. True to blacklist owner of the chip
---@return Entity[] players Array of matchd players
local function get(whitelist, blacklist)
	local matched
	if whitelist == true or whitelist == nil then
		matched = find.allPlayers()
	else
		if type(whitelist) ~= "table" then whitelist = { whitelist } end
		matched = {}
		for _, ply in ipairs(find.allPlayers()) do
			for _, identifier in ipairs(whitelist) do
				if match(ply, identifier) then
					matched[#matched+1] = ply
				end
			end
		end
	end
	
	if type(blacklist) == true then
		for i = #matched, 1, -1 do
			local ply = matched[i]
			if ply == owner() then
				table.remove(matched, i)
			end
		end
	else
		if type(blacklist) ~= "table" then blacklist = { blacklist } end
		for i = #matched, 1, -1 do
			for _, identifier in ipairs(blacklist) do
				if match(matched[i], identifier) then
					table.remove(matched, i)
				end
			end
		end
	end
	
	return matched
end

return get
