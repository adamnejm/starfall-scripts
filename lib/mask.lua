--@name Mask Stencil Library
--@author Name

--- Prepares the stencil for drawing the clipping mask
local function push()
	render.resetStencil()
	render.setStencilEnable(true)
	render.setStencilReferenceValue(1)
	render.setStencilCompareFunction(STENCIL.NEVER)
	render.setStencilFailOperation(STENCIL.REPLACE)
end

--- Confirms the clipping mask and prepares stencil for drawing the contents
---@param invert boolean True to invert the mask
local function clip(invert)
	render.setStencilCompareFunction(invert and STENCIL.NOTEQUAL or STENCIL.EQUAL)
	render.setStencilFailOperation(STENCIL.KEEP)
end

--- Finishes clipping mask and disables stencil
local function pop()
	render.setStencilEnable(false)
end

-------------------------------------------

return {
	push = push,
	clip = clip,
	pop  = pop,
}
