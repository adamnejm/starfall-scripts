--@name Bone Explorer
--@author Name
--@client

local owner, client, chip, world = owner(), player(), chip(), entity(0)

-------------------------------------------

local font = render.createFont("Roboto", 22, 500, true, false, false, true)

hook.add("DrawHUD", "", function()
	local ent
	local view_ent = client:getViewEntity()
	if view_ent ~= client and view_ent:getClass() == "gmod_cameraprop" then
		ent = client
	else
		ent = trace.hull(eyePos(), eyePos() + eyeVector() * 500, Vector(-10), Vector(10)).Entity
	end
	if not ent or not ent:isValid() then return end
	
	local cursor
	if input.getCursorVisible() then
		cursor = Vector(input.getCursorPos())
	else
		cursor = Vector(render.getResolution()) / 2
	end
	
	local offset = 0
	for i = 0, ent:getBoneCount() - 1 do
		local pos = ent:getBoneMatrix(i)
		if not pos then return end
		pos = pos:getTranslation()
		local screen_pos = pos:toScreen()
		
		render.setColor(Color(360 / ent:getBoneCount() * i, 1, 1):hsvToRGB())
		render.drawFilledCircle(screen_pos.x, screen_pos.y, 5)
		
		local parent = ent:getBoneParent(i)
		if parent > -1 then
			local parent_pos = ent:getBoneMatrix(parent):getTranslation()
			local parent_screen_pos = parent_pos:toScreen()
			render.drawLine(screen_pos.x, screen_pos.y, parent_screen_pos.x, parent_screen_pos.y)
		end
		
		local d = cursor:getDistance(Vector(screen_pos.x, screen_pos.y))
		if d < 50 then
			render.setFont(font)
			render.setColor(Color(360 / ent:getBoneCount() * i, 0.4, 1):hsvToRGB())
			render.drawSimpleText(cursor.x + 10, cursor.y + offset, string.format("[%i] - %s", i, ent:getBoneName(i)), TEXT_ALIGN.LEFT, TEXT_ALIGN.CENTER)
			offset = offset + 20
		end
	end
end)

if client == owner then
	enableHud(nil, true)
end
