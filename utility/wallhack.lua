--@name Wallhack
--@author Name
--@shared

local owner, client, chip, world = owner(), player(), chip(), entity(0)

-------------------------------------------

local info_matrix_scale = 0.3
local info_matrix_distance_max = 1000

-------------------------------------------

if SERVER then
	
	local dormant = {}
	
	hook.add("PlayerInitialSpawn", "UpdatePlayers", function(ply)
		net.start("player_spawn")
		net.writeEntity(ply)
		net.send(owner)
	end)
	hook.add("PlayerDisconnected", "UpdatePlayers", function(ply)
		table.removeByValue(dormant, ply)
		net.start("player_left")
		net.send(owner)
	end)
	
	local net_name = "dormant"
	local net_overhead = 8 * 8 + 8 * (#net_name + 1)
	
	hook.add("Tick", "", function()
		local net_size = net_overhead + (1 + 8 + 7) * #dormant
		if net.getBytesLeft() * 8 < net_size then return end
		if #dormant > 0 then
			net.start(net_name)
			net.writeUInt(#dormant, 8)
			for _, ply in ipairs(dormant) do
				net.writeUInt(ply:getUserID(), 8)
				net.writeVector(ply:getPos())
				net.writeUInt(math.clamp(ply:getHealth(), 0, 100), 7)
			end
			net.send(owner, true)
		end
	end)
	
	net.receive("dormant", function()
		for _ = 1, net.readUInt(8) do
			local is_dormant = net.readBool()
			local ply = player(net.readUInt(8))
			if is_dormant then
				dormant[#dormant+1] = ply
			else
				table.removeByValue(dormant, ply)
			end
		end
	end)
	
elseif client == owner then
	
	local tracked = {}
	local tracked_lookup = {}
	local function track_player(ply)
		local holo = hologram.create(Vector(), Angle(), ply:getModel(), Vector())
		holo:addEffects(EF.BONEMERGE)
		holo:setParent(ply)
		holo:setNoDraw(true)
		
		local data = {
			dormant = nil,
			pos     = ply:getPos(),
			health  = ply:getHealth(),
			
			user_id = ply:getUserID(),
			ply     = ply,
			holo    = holo,
		}
		tracked[#tracked + 1] = data
		tracked_lookup[ply:getUserID()] = data
	end
	
	find.allPlayers(function(ply)
		-- if ply ~= owner then track_player(ply) end
		track_player(ply)
	end)
	net.receive("player_spawn", function()
		net.readEntity(track_player)
	end)
	net.receive("player_left", function()
		for i = #tracked, 1, -1 do
			local data = tracked[i]
			if not data.ply:isValid() then
				data.holo:remove()
				table.remove(tracked, i)
				tracked_lookup[data.user_id] = nil
			end
		end
	end)
	
	net.receive("dormant", function()
		for _ = 1, net.readUInt(8) do
			local data = tracked_lookup[net.readUInt(8)]
			data.pos = net.readVector()
			data.health = net.readUInt(7)
		end
	end)
	
	-------------------------------------------
	
	local font_big = render.createFont("Roboto-Medium.ttf", 72, 500, true)
	local font_small = render.createFont("Roboto-Medium.ttf", 42, 500, true)
	
	local function draw_text_shadow(x, y, text, offset, font, text_color, shadow_color, align_x, align_y)
		render.setFont(font)
		render.setColor(shadow_color)
		render.drawSimpleText(x + offset, y + offset, text, align_x, align_y)
		render.setColor(text_color)
		render.drawSimpleText(x, y, text, align_x, align_y)
	end
	
	local function draw_info(ply, data)
		draw_text_shadow(0, 0, ply:getName(), 3, font_big, Color(255, 255, 255), Color(0, 0, 0), 1, 0)
		draw_text_shadow(0, 58, "Health: " .. data.health, 2, font_small, Color(255, 255, 255), Color(0, 0, 0), 1, 0)
	end
	
	local wireframe_material = material.create("UnlitGeneric")
	wireframe_material:setInt("$flags", 0x10000000)
	local wireframe_entity_material = "!" .. wireframe_material:getName()
	
	local solid_material = material.create("UnlitGeneric")
	solid_material:setInt("$flags", 0x2000)
	local solid_entity_material = "!" .. solid_material:getName()
	
	local function draw_outline(holo)
		render.setStencilCompareFunction(STENCIL.NEVER)
		render.setStencilFailOperation(STENCIL.REPLACE)
			holo:setMaterial(wireframe_entity_material)
			holo:draw()
		render.setStencilCompareFunction(STENCIL.NEVER)
		render.setStencilFailOperation(STENCIL.ZERO)
			holo:setMaterial(solid_entity_material)
			holo:draw()
		render.setStencilCompareFunction(STENCIL.EQUAL)
		render.clearBuffersObeyStencil(255, 55, 255, 0, false)
	end
	
	-------------------------------------------
	
	local dormant_queue = {}
	hook.add("PreDrawViewModels", "DrawOutlines", function()
		render.setFilterMag(3)
		render.setFilterMin(3)
		
		render.resetStencil()
		render.setStencilReferenceValue(1)
		
		for _, data in ipairs(tracked) do
			local ply = data.ply
			if ply:isValid() and ply:isAlive() and client:getViewEntity() ~= ply then
				
				local is_dormant = ply:isDormant()
				if is_dormant ~= data.dormant then
					data.dormant = is_dormant
					dormant_queue[#dormant_queue+1] = ply
				end
				if not is_dormant then
					data.pos = ply:getPos()
					data.health = ply:getHealth()
				end
				
				render.setStencilEnable(true)
				draw_outline(data.holo)
				render.setStencilEnable(false)
				
				local origin = data.pos - Vector(0, 0, 5)
				local distance = eyePos():getDistance(origin)
				
				local info_matrix = Matrix()
				info_matrix:setTranslation(eyePos() + (origin - eyePos()):getNormalized() * math.min(distance, info_matrix_distance_max))
				info_matrix:setAngles((eyePos() - info_matrix:getTranslation()):getAngle() + Angle(90, 0, 0))
				info_matrix:rotate(Angle(0, 90, 0))
				info_matrix:setScale(Vector(info_matrix_scale, -info_matrix_scale))
				
				render.pushMatrix(info_matrix)
					draw_info(ply, data)
				render.popMatrix()
			end
		end
		
		if #dormant_queue > 0 then
			net.start("dormant")
			net.writeUInt(#dormant_queue, 8)
			for i = #dormant_queue, 1, -1 do
				local ply = dormant_queue[i]
				net.writeBool(ply:isDormant())
				net.writeUInt(ply:getUserID(), 8)
				dormant_queue[i] = nil
			end
			net.send()
		end
		
		render.setStencilEnable(false)
	end)
	
	enableHud(nil, true)
	
end
