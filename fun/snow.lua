--@name Snow
--@author Name
--@client

local owner, client, chip, world = owner(), player(), chip(), entity(0)

-------------------------------------------

-- Comment the line below to enable snow for all players
if client ~= owner then return end

-------------------------------------------

local emitters = {}
for i = 1, particle.particleEmittersLeft() do
	local emitter = particle.create(chip:getPos(), false)
	emitter:setNearClip(10, 100)
	emitters[i] = emitter
end

local mat = material.load("particle/snow")
local height  = Vector(0, 0, 250)
local gravity = Vector(0, 0, 220)

timer.create("", 0.05, 0, function()
	if cpuAverage() > cpuMax() * 0.9 then return end
	
	local vel = client:getVelocity()
	local vel_plane = Vector(math.clamp(vel.x, -250, 250), math.clamp(vel.y, -250, 250), vel.z) / 2
	local origin = client:getPos() + height + client:getAimVector():setZ(0) * vel_plane:getLength() + vel_plane
	
	for _, emitter in ipairs(emitters) do
		if emitter:getParticlesLeft() > 0 then
			local size_start  = math.rand(0.8, 2.6)
			local size_finish = math.rand(0.3, 0.6)
			local offset = Vector(-0.5 + math.random(), -0.5 + math.random()):getNormalized() * math.rand(50, 300)
			
			local part = emitter:add(mat, origin + offset, size_start, size_finish, size_start, size_finish, 0, 255, 1.5)
			part:setGravity(gravity)
			part:setVelocity(Vector(math.rand(-3, 3), math.rand(-3, 3), -30) * 10)
			part:setAirResistance(-10)
			part:setRoll(1)
		end
	end
end)
