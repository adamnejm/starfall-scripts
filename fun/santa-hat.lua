--@name Santa Hat
--@author Name
--@shared

local owner, client, chip, world = owner(), player(), chip(), entity(0)

-------------------------------------------

local target = owner
-- local target = find.playersByName("Name")[1]

-------------------------------------------

if SERVER then
	local eyes = hologram.create(chip:getPos(), chip:getAngles(), "models/holograms/cube.mdl", Vector(0.3))
	eyes:setMaterial("models/wireframe")
	eyes:setNoDraw(true)
	
	local base = hologram.create(eyes:localToWorld(Vector(-3.5, 0, 2)), eyes:localToWorldAngles(Angle(-30, 0, 0)), "models/holograms/cube.mdl", Vector(0.3))
	base:setMaterial("models/wireframe")
	base:setNoDraw(true)
	base:setParent(eyes)
	
	local holos = { [0] = base }
	for i = 1, 10 do
		local prev = holos[i - 1]
		local holo = hologram.create(prev:localToWorld(Vector(0, 0, 1.1)), prev:localToWorldAngles(Angle(-10, -5, 0)), "models/holograms/hq_sphere.mdl", Vector(0.85, 0.65, 0.75) * (1 - i * 0.08))
		holo:setMaterial("models/XQM/Rails/gumball_1")
		holo:setColor(Color(203, 36, 30))
		holo:setParent(prev)
		holos[i] = holo
	end
	
	local last = holos[#holos]
	local ball = hologram.create(last:localToWorld(Vector(0, 0, 2)), last:localToWorldAngles(Angle(0, 0, 0)), "models/holograms/hq_sphere.mdl", Vector(0.3))
	ball:setColor(Color(254, 253, 255))
	ball:setMaterial("models/XQM/Rails/gumball_1")
	ball:setParent(last)
	
	local frot = hologram.create(base:localToWorld(Vector(0, 0, 0)), base:localToWorldAngles(Angle(0, 0, 0)), "models/holograms/hq_rcylinder_thick.mdl", Vector(0.85, 0.65, 0.25))
	frot:setColor(Color(254, 253, 255))
	frot:setMaterial("models/XQM/Rails/gumball_1")
	frot:setParent(base)
	
	-------------------------------------------
	
	local eye_yaw_old = 0
	local eye_yaw_delta = 0
	timer.create("Animate", 1 / 10, 0, function()
		local vel = target:getLocalVelocity() / 25
		local eye_yaw = target:getEyeAngles().yaw
		eye_yaw_delta = (eye_yaw_delta + math.normalizeAngle(eye_yaw_old - eye_yaw) / 10) * 0.1
		eye_yaw_old = eye_yaw
		
		for i, holo in ipairs(holos) do
			local vertical = math.clamp(-vel.z * 2, -10, 10)
			local horizontal = math.sin(timer.curtime() * math.min(math.abs(vel.x), 100) * 10)
			local pitch = math.clamp((-vel.x + vertical) - i * 1.2, -10, 1)
			local roll = math.clamp(vel.y + horizontal + eye_yaw_delta * i, -10, 10)
			local ang = Angle(pitch, 0, roll)
			holo:setAngles(holos[i - 1]:localToWorldAngles(ang))
		end
	end)
	
	timer.create("ParentFix", 1, 0, function()
		if target:getVelocity():getLength() > 0 then return end
		-- Re-parent constantly to fix attachment offset bug
		local pos, ang = target:getAttachment(target:lookupAttachment("eyes"))
		eyes:setPos(pos)
		eyes:setAngles(ang)
		eyes:setParent(target, "eyes")
	end)
	
	hook.add("ClientInitialized", "SendHolograms", function(ply)
		if ply ~= target then return end
		net.start("holos")
			net.writeEntity(ball)
			net.writeEntity(frot)
			for _, holo in ipairs(holos) do
				net.writeEntity(holo)
			end
		net.send(ply)
	end)
	
elseif client == target then
	
	local holos = {}
	local function read_holo(holo)
		holos[#holos+1] = holo
		if target:getViewEntity() == target then
			holo:setNoDraw(true)
		end
	end
	
	net.receive("holos", function()
		for i = 1, 12 do
			net.readEntity(read_holo)
		end
	end)
	
	local view_entity
	timer.create("ToggleVisibility", 0.1, 0, function()
		local current_view_entity = target:getViewEntity()
		if current_view_entity ~= view_entity then
			view_entity = current_view_entity
			
			local no_draw = view_entity == target
			for _, holo in ipairs(holos) do
				if holo:isValid() then
					holo:setNoDraw(no_draw)
				end
			end
		end
	end)
	
end
