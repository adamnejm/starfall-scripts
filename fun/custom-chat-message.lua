--@name Custom Chat Rainbow
--@author Name
--@server

local owner, client, chip, world = owner(), player(), chip(), entity(0)

-------------------------------------------

-- This script is meant to be used along with the Custom Chat addon:
-- https://steamcommunity.com/sharedfiles/filedetails/?id=2799307109

hook.add("PlayerSay", "", function(ply, text)
    if ply ~= owner then return end
    if string.sub(text, 1, 1) == "!" then return end
    return string.format(" <a:pet_the_smoothbrain:843655976363360286>;lucida;$$ %s $$", text)
end)
