--@name Flying Penis
--@author Name
--@server

local owner, client, chip, world = owner(), player(), chip(), entity(0)


-------------------------------------------
--               SETTINGS                --
-------------------------------------------

-- General
local fps = 20                                                 -- Simulation target frames per second

local scale             = 1.5                                  -- Global scale of the penis
local length            = 16                                   -- Length of the shaft
local height            = 0.25                                 -- Length of individual shaft segments
local speed             = 100 * scale                          -- Maximum speed of the penis
local turn              = 0.1                                  -- How fast to turn towards the target
local slither_angle     = 10                                   -- How much the snake should turn during slithering
local slither_speed     = 5                                    -- Slithering speed
local cum_distance      = 50 * scale                           -- Distance from target at which to slow down, also controls freeroam target distance check
local cum_delay         = 20                                   -- How often can the penis cum on people
local cum_amount        = 20                                   -- Load amount
local cum_duration      = 1.8                                  -- Cumshot duration
local cum_bind          = nil                                  -- Key to manually unload, eg. IN_KEY.USE

-- Targeting
local player_name       = ""                                   -- If set, will harass player with the given nickname, otherwise will float around a chip and target random people
local player_range      = 750 * scale                          -- Search radius for a player to cum on
local target_range      = 200 * scale                          -- Range to fly around within around the target
local allow_owner       = false                                -- Whether to also cum on the chip owner

-- Appearance
local suppress_lighting = false                                -- Whether to disable the default engine shading
local head_color        = Color(255, 127, 223)                 -- Tip color
local head_material     = "phoenix_storms/mat/mat_phx_plastic" -- Tip material
local skin_color        = Color(246, 219, 142)                 -- Shaft color
local skin_material     = "phoenix_storms/egg"                 -- Shaft material
local ball_color        = Color(232, 203, 124)                 -- Testes color
local ball_material     = "phoenix_storms/egg"                 -- Testes material
local cum_color         = Color(255, 255, 255)                 -- Cum color
local cum_splatter      = "PaintSplatPink"                     -- Cum splatter texture
local cum_drop_sounds   = {                                    -- Sounds of cum when hitting the ground, chosen at random
	"player/footsteps/mud1.wav",
	"player/footsteps/mud2.wav",
	"player/footsteps/mud3.wav",
	"player/footsteps/mud4.wav",
	"ambient/levels/canals/toxic_slime_gurgle5.wav",
}


-------------------------------------------
--               HOLOGRAMS               --
-------------------------------------------

local head = hologram.create(chip:getPos(), chip:getAngles(), "models/holograms/hq_dome.mdl", scale * Vector(1, 1, 2.2))
head:setColor(head_color)
head:setMaterial(head_material)
head:suppressEngineLighting(suppress_lighting)
head:setParent(chip)

local head_ring = hologram.create(head:getPos(), head:getAngles(), "models/holograms/hq_torus_thick.mdl", scale * Vector(1.01, 1.01, 1.5))
head_ring:setColor(head_color)
head_ring:setMaterial(head_material)
head_ring:suppressEngineLighting(suppress_lighting)
head_ring:setParent(head)

local head_connector = hologram.create(head:getPos(), head:getAngles(), "models/holograms/hq_sphere.mdl", scale * Vector(0.9))
head_connector:setColor(skin_color)
head_connector:setMaterial(skin_material)
head_connector:suppressEngineLighting(suppress_lighting)
head_connector:setParent(head)

local tail = { [0] = head }
for i = 1, length do
	local prev = tail[i - 1]
	local segment = hologram.create(prev:getPos() - prev:getUp() * 12 * height * scale, prev:getAngles(), "models/holograms/hq_cylinder.mdl", scale * Vector(0.9, 0.9, height))
	segment:setColor(skin_color)
	segment:setMaterial(skin_material)
	segment:suppressEngineLighting(suppress_lighting)
	tail[#tail+1] = segment
	
	local segment_connector = hologram.create(segment:getPos() - segment:getUp() * 6 * height * scale, prev:getAngles(), "models/holograms/hq_sphere.mdl", scale * Vector(0.9))
	segment_connector:setColor(skin_color)
	segment_connector:setMaterial(skin_material)
	segment_connector:suppressEngineLighting(suppress_lighting)
	segment_connector:setParent(segment)
	segment.connector = segment_connector
end
local last = tail[#tail]

local ball_left = hologram.create(last:localToWorld(Vector(3, 7, -height * 6) * scale), last:localToWorldAngles(Angle(30, 0, -20)), "models/holograms/hq_sphere.mdl", scale * Vector(1.4, 1.4, 1.8) * 0.9)
ball_left:setColor(ball_color)
ball_left:setMaterial(ball_material)
ball_left:suppressEngineLighting(suppress_lighting)
ball_left:setParent(last)

local ball_right = hologram.create(last:localToWorld(Vector(3, -7, -height * 6) * scale), last:localToWorldAngles(Angle(30, 0, 20)), "models/holograms/hq_sphere.mdl", scale * Vector(1.4, 1.4, 1.8) * 0.9)
ball_right:setColor(ball_color)
ball_right:setMaterial(ball_material)
ball_right:suppressEngineLighting(suppress_lighting)
ball_right:setParent(last)


-------------------------------------------
--                 VARS                  --
-------------------------------------------

local target = chip:getPos() + chip:getUp() * 30 * scale
local turn_smoothing = 0

local player, player_cum = nil, nil
if player_name and player_name ~= "" then
	player = find.playersByName(player_name, false)[1]
	if player then
		print("Harassing player '" .. player:getName() .. "'")
	end
end

local cum_drops = {}
local cum_emitter = effect.create()

-------------------------------------------

local function angnorm(ang)
	return Angle((ang[1] + 180) % 360 - 180, (ang[2] + 180) % 360 - 180, (ang[3] + 180) % 360 - 180)
end


-------------------------------------------
--              SIMULATION               --
-------------------------------------------

local function cum()
	timer.remove("Cum")
	timer.create("Cum", 0, cum_amount, function()
		local tip_pos = head:localToWorld(Vector(0, 0, 6 * 2.2 * scale))
		
		local drop = hologram.create(head:getPos(), chip:getAngles(), "models/holograms/hq_sphere.mdl", scale * Vector(0.5))
		drop:setColor(cum_color)
		drop:setTrails(10 * scale / 2, 0, 0.01 * scale, "effects/beam_generic01", cum_color)
		drop:setVel(head:getUp() * math.rand(350, 500) * scale + head:getForward() * math.rand(-100, 100) + head:getRight() * math.rand(-100, 100))
		cum_drops[#cum_drops+1] = drop
		
		if effect.canCreate() then
			cum_emitter:setOrigin(tip_pos)
			cum_emitter:setNormal(head:getUp())
			cum_emitter:setScale(scale)
			cum_emitter:setMagnitude(1.2)
			cum_emitter:play("ElectricSpark")
		end
		
		timer.adjust("Cum", (1 - timer.repsleft("Cum") / cum_amount) * cum_duration / cum_amount)
	end)
end

local function simulate_cum(dt)
	for i = #cum_drops, 1, -1 do
		local drop = cum_drops[i]
		local drop_pos = drop:getPos()
		local drop_velocity = drop:getVelocity()
		
		if drop.keep then
			drop.keep = drop.keep - dt
			if drop.keep < 0 then
				table.remove(cum_drops, i):remove()
			end
		else
			local ray = trace.line(drop_pos, drop_pos + drop_velocity / 10)
			if ray.Hit then
				trace.decal(cum_splatter, ray.HitPos + ray.HitNormal * 100, ray.HitPos - ray.HitNormal * 1000)
				
				if sound.canEmitSound() then
					drop:emitSound(cum_drop_sounds[math.random(#cum_drop_sounds)], 75, 130, 1)
				end
				
				drop:setPos(ray.HitPos)
				drop:setVel(Vector())
				drop:setColor(Color(0,0,0,0))
				drop.keep = 1 -- Need to keep the hologram for a bit, otherwise `emitSound` is not gonna work
			else
				drop:setVel(drop_velocity + Vector(0, 0, -9.8) * 100 * scale * dt)
			end
		end
	end
end

local function simulate_head(dt)
	local head_pos = head:getPos()
	local target_pos = head_pos + head:getUp() * speed * dt
	head:setPos(target_pos)
	
	local target_ang = ((target - head_pos):getAngle() + Angle(90, 0, 0))
	target_ang = head:getAngles() + angnorm(target_ang - head:getAngles()) * turn_smoothing
	target_ang = target_ang + Angle(0, math.sin(timer.curtime() * slither_speed) * slither_angle, 0)
	head:setAngles(target_ang)
	
	turn_smoothing = math.min(turn_smoothing + dt * turn, 1)
end

local function simulate_tail()
	for i = 1, #tail do
		local prev = tail[i - 1]
		local holo = tail[i]
		
		local holo_pos = holo:getPos()
		local prev_pos = i == 1 and prev:getPos() or prev:getPos() - prev:getUp() * 6 * height * scale
		
		holo:setPos(prev_pos + (holo_pos - prev_pos):getNormalized() * 6 * height * scale)
		holo:setAngles((prev_pos - holo_pos):getAngle() + Angle(90, 0, 0))
	end
end


-------------------------------------------
--               BEHAVIOR                --
-------------------------------------------

local behavior

local function freeroam()
	local tip_pos = head:localToWorld(Vector(0, 0, 6 * 2.2 * scale))
	local distance = target:getDistance(tip_pos)
	
	if distance < cum_distance then
		
		local origin
		if player and player:isValid() then
			origin = player:getShootPos()
		else
			origin = chip:getPos()
		end
		
		for _ = 1, 50 do
			target = origin + Vector(math.rand(-target_range, target_range), math.rand(-target_range, target_range), math.rand(-target_range / 2, target_range / 2))
			if target:isInWorld() then
				break
			end
		end
		
		turn_smoothing = 0
	end
	
	local ang = Angle()
	local rotation = timer.curtime() * 10
	local hit_count = 0
	for i = 1, 4 do
		local x = math.sin(rotation + math.pi * 2 / 4 * i)
		local y = math.cos(rotation + math.pi * 2 / 4 * i)
		local ray = trace.line(head:getPos(), head:localToWorld(Vector(x, y, 1) * 50 * scale))
		if ray.Hit then
			ang = ang + Angle(-x, 0, y) * (1 - ray.Fraction) * 20
			hit_count = hit_count + 1
		end
	end
	
	if hit_count > 0 and head:getPos():isInWorld() then
		turn_smoothing = 0.05
		head:setAngles(head:getAngles() + ang * hit_count)
		
		target = target + (head:getPos() - target):getNormalized() * 5
	end
end

local function orgasm()
	local tip_pos = head:localToWorld(Vector(0, 0, 6 * 2.2 * scale))
	if player_cum:isValid() then
		target = player_cum:getShootPos()
		if player_cum:getPos():getDistance(tip_pos + head:getUp() * cum_distance) <= cum_distance * 2 then
			cum()
			player_cum = nil
			behavior = freeroam
		elseif not player and player_cum:getPos():getDistance(tip_pos) > player_range * 2 then
			behavior = freeroam
		end
	else
		behavior = freeroam
	end
end

behavior = freeroam


-------------------------------------------
--                 LOGIC                 --
-------------------------------------------

local previous_time = timer.curtime()
timer.create("Animate", 1 / fps, 0, function()
	local current_time = timer.curtime()
	local dt = current_time - previous_time
	previous_time = current_time
	
	behavior()
	simulate_head(dt)
	simulate_tail(dt)
	simulate_cum(dt)
end)

timer.create("Orgasm", cum_delay, 0, function()
	if behavior == orgasm then return end
	
	if player then
		player_cum = player
	else
		local players = find.inSphere(head:getPos(), player_range, function(ent)
			return allow_owner and ent:isPlayer() or (ent:isPlayer() and ent ~= owner)
		end)
		player_cum = players[math.random(#players)]
	end
	if not player_cum or not player_cum:isValid() then return end
	
	behavior = orgasm
	turn_smoothing = 0
	timer.adjust("Orgasm", math.random(cum_delay, cum_delay * 2))
end)

if cum_bind then
	hook.add("KeyPress", "CumBind", function(ply, key)
		if ply ~= owner or key ~= cum_bind then return end
		cum()
	end)
end
