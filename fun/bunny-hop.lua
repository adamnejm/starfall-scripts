--@name Bunny-Hop
--@author Name
--@client

local owner, client, chip, world = owner(), player(), chip(), entity(0)

-------------------------------------------

local run          = true -- Whether to automatically while bunny-hopping
local jump_button  = input.lookupBinding("+jump")
local speed_button = input.lookupBinding("+speed")

-------------------------------------------

if client ~= owner then return end

local enabled, in_air = false, false
local function enable()
	enabled, in_air = true, true
	if run and not input.isKeyDown(speed_button) then concmd("+speed") end
	hook.add("Tick", "BunnyHop", function()
		if owner:isOnGround() and in_air then
			concmd("+jump")
			in_air = false
		elseif not in_air then
			concmd("-jump")
			in_air = true
		end
	end)
end

local function disable()
	enabled = false
	if run and not input.isKeyDown(speed_button) then concmd("-speed") end
	hook.remove("Tick", "BunnyHop")
end

-------------------------------------------

hook.add("InputPressed", "", function(key)
	if key ~= jump_button or owner:isNoclipped() or owner:inVehicle() then return end
	enable()
end)

hook.add("InputReleased", "", function(key)
	if key == jump_button then
		disable()
	elseif key == speed_button and run and (enabled or owner:isNoclipped()) then
		concmd("-speed")
	end
end)

hook.add("PlayerNoClip", "", function(_, noclip)
	if not isFirstTimePredicted() then return end
	disable()
end)
