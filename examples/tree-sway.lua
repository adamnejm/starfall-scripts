--@name Tree Sway Example
--@author Name
--@client

local owner, client, chip, world = owner(), player(), chip(), entity(0)

-------------------------------------------

local holo = hologram.create(chip:getPos() + chip:getUp() * 30, chip:getAngles(), "models/props_borealis/bluebarrel001.mdl")
local mat = material.create("VertexLitGeneric")
mat:setTexture("$basetexture", "models/props_borealis/bluebarrel001")

mat:setInt("$treesway", 2)

-- Make it work without 'env_wind'
mat:setFloat("$treeswaystatic", 1)

-- [0..1] - Changes area(?) the 'scrumble' functions affect
-- >1 - Also moves the entire model according to various treesway parameters
mat:setFloat("$treeswaystartheight", 1.5)
mat:setFloat("$treeswayspeed", 0.5)

-- Optional stuff that affects individual vertices, requires $treesway set to 2
mat:setFloat("$treeswayscrumblespeed", 1)
mat:setFloat("$$treeswayscrumblefrequency", 10.1)
mat:setFloat("$treeswayscrumblestrength", 5)

holo:setMaterial("!" .. mat:getName())
