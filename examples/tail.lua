--@name Tail Example
--@author Name
--@server

local owner, client, chip, world = owner(), player(), chip(), entity(0)

-------------------------------------------

local target = chip
local length = 16
local spacing = 6
local update_rate = 1 / 12

-------------------------------------------

local tail = { [0] = target }
for i = 1, length do
	local holo = hologram.create(chip:getPos() + chip:getUp() * spacing * i, chip:getAngles(), "models/holograms/hq_rcylinder_thick.mdl")
	holo:setMaterial("models/props_lab/warp_sheet")
	holo:setColor(Color(120 + 90 / length * i, 1, 1):hsvToRGB())
	holo:suppressEngineLighting(true)
    tail[i] = holo
end

local last_update = 0
hook.add("Tick", "", function()
    if timer.curtime() < last_update then return end
    last_update = timer.curtime() + update_rate
    
    for i = 1, length do
        local current = tail[i]
        local previous = tail[i-1]
        
        local pos = previous:getPos() + (current:getPos() - previous:getPos()):getNormalized() * spacing
        local ang = (previous:getPos() - current:getPos()):getAngle() + Angle(90, 0, 0)
        current:setPos(pos)
        current:setAngles(ang)
    end
end)
