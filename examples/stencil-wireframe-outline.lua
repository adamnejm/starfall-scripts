--@name Stencil Wireframe Outline Example
--@author Name
--@client

local owner, client, chip, world = owner(), player(), chip(), entity(0)

-- Spawn some NPCs and place the chip, an outline will be added to them
-- This technique exploits the fact that wireframe shader renders one pixel outwards

local xray = false -- Whether to show the outline through walls

-------------------------------------------

local mat = material.create("UnlitGeneric")
mat:setInt("$flags", 0x10000000 + (xray and 0x8000 or 0))
local model_mat = "!" .. mat:getName()

local holo = hologram.create(Vector(), Angle(), "models/holograms/cube.mdl")
holo:addEffects(EF.BONEMERGE)
holo:setNoDraw(true)

-------------------------------------------

hook.add("PreDrawViewModels", "DrawOutlines", function()
	render.resetStencil()
    render.setStencilEnable(true)
	
	local ents = find.byClass("npc_*", function(ent)
		return ent:getOwner() == owner and not ent:isDormant()
	end)
	
	for _, ent in ipairs(ents) do
		render.clearStencil()
		
		render.setStencilReferenceValue(1)
		render.setStencilCompareFunction(STENCIL.NEVER)
		render.setStencilFailOperation(STENCIL.REPLACE)
		
		holo:setModel(ent:getModel())
		holo:setParent(ent)
		holo:setMaterial("")
		holo:draw(true)
		
		render.setStencilCompareFunction(STENCIL.NOTEQUAL)
		
		holo:setMaterial(model_mat)
		holo:setColor(Color(ent:entIndex() * 16180339, 0.6, 0.9):hsvToRGB())
		holo:draw()
	end
	
	render.setStencilEnable(false)
end)

if client == owner then
	enableHud(nil, true)
end
