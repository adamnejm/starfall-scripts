--@name Fog Controller
--@author Name
--@client

local owner, client, chip, world = owner(), player(), chip(), entity(0)

-------------------------------------------

local density, distance = 0, 0
local function setupFog(scale)
	-- Distances have to be corrected according to skybox's scale
	local skybox_mul = scale or 1
	
	-- Only calculate fog properties once, in SetupWorldFog hook
	if not scale then
		distance = chip:getPos():getDistance(owner:getPos())
		density = 1 - math.clamp(distance / 500, 0, 1)
	end
	
	render.setFogMode(MATERIAL_FOG.LINEAR)
	render.setFogColor(Color(230, 245, 255))
	
	-- Thickens the fog when you get closer to the chip
	render.setFogDensity(density)
	render.setFogStart(distance / 500 * skybox_mul)
	render.setFogEnd((distance + 500) * skybox_mul)
end

hook.add("SetupWorldFog", "", setupFog)
hook.add("SetupSkyboxFog", "", setupFog)

-------------------------------------------

if client == owner then
	enableHud(nil, true)
end
