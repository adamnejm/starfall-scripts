--@name Clipping Mask Example
--@author Name
--@client
--@include ../lib/mask.lua

local mask = require "../lib/mask.lua"

-------------------------------------------

local face_material = material.create("UnlitGeneric")
face_material:setTexture("$basetexture", "models/gman/gman_facehirez")

local background_material = material.create("UnlitGeneric")
background_material:setTexture("$basetexture", "phoenix_storms/wood")

render.createRenderTarget("canvas")

hook.add("Render", "", function()
	render.selectRenderTarget("canvas")
		render.clear(Color(0, 0, 0, 0))
		mask.push()
			-- Anything drawn here will act like a clipping mask
			local x, y = render.cursorPos()
			if x and y then
				render.drawFilledCircle(x * 2, y * 2, 128)
			end
		mask.clip()
			-- Contents to be affected by the clipping mask
			-- You can also, pass `true` to the `mask.clip()` to invert the mask
			render.setMaterial(face_material)
			render.drawTexturedRect(0, 0, 1024, 1024)
		mask.pop()
	render.selectRenderTarget()
	
	render.setMaterial(background_material)
	render.drawTexturedRect(0, 0, 512, 512)
	
	render.setColor(Color(255, 255, 255))
	render.setRenderTargetTexture("canvas")
	render.drawTexturedRect(0, 0, 512, 512)
end)
