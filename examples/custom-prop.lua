--@name Custom Prop Example
--@author Name
--@shared

local owner, client, chip, world = owner(), player(), chip(), entity(0)

-------------------------------------------

local x, y, z = 64, 32, 16

if SERVER then
	local convexes = {
		{
			Vector(0, 0, 0), Vector(x, 0, 0), Vector(x, y, 0), Vector(0, y, 0),
			Vector(0, 0, z), Vector(x, 0, z), Vector(x, y, z), Vector(0, y, z),
		}
	}
	local ent = prop.createCustom(chip:getPos() + chip:getUp() * 20, Angle(), convexes, true)
	ent:setMass(200)
	
	local ent2 = prop.createCustom(chip:getPos() + chip:getUp() * 20, Angle(), convexes, true)
	ent2:setMass(200)
	
	hook.add("ClientInitialized", "", function(ply)
		net.start("ent")
			net.writeEntity(ent)
		net.send(ply)
	end)
else
	local vertices = {
		{ pos = Vector(0, 0, z), u = 0, v = 0, normal = Vector(0, 0, 1)  }, { pos = Vector(x, y, z), u = 1, v = 1, normal = Vector(0, 0, 1)  }, { pos = Vector(x, 0, z), u = 1, v = 0, normal = Vector(0, 0, 1)  },
		{ pos = Vector(0, 0, z), u = 0, v = 0, normal = Vector(0, 0, 1)  }, { pos = Vector(0, y, z), u = 0, v = 1, normal = Vector(0, 0, 1)  }, { pos = Vector(x, y, z), u = 1, v = 1, normal = Vector(0, 0, 1)  },
		{ pos = Vector(0, 0, 0), u = 0, v = 0, normal = Vector(0, 0, -1) }, { pos = Vector(x, 0, 0), u = 1, v = 0, normal = Vector(0, 0, -1) }, { pos = Vector(x, y, 0), u = 1, v = 1, normal = Vector(0, 0, -1) },
		{ pos = Vector(0, 0, 0), u = 0, v = 0, normal = Vector(0, 0, -1) }, { pos = Vector(x, y, 0), u = 1, v = 1, normal = Vector(0, 0, -1) }, { pos = Vector(0, y, 0), u = 0, v = 1, normal = Vector(0, 0, -1) },
		{ pos = Vector(0, y, 0), u = 0, v = 1, normal = Vector(0, 1, 0)  }, { pos = Vector(x, y, 0), u = 1, v = 1, normal = Vector(0, 1, 0)  }, { pos = Vector(0, y, z), u = 0, v = 0, normal = Vector(0, 1, 0)  },
		{ pos = Vector(0, y, z), u = 0, v = 0, normal = Vector(0, 1, 0)  }, { pos = Vector(x, y, 0), u = 1, v = 1, normal = Vector(0, 1, 0)  }, { pos = Vector(x, y, z), u = 1, v = 0, normal = Vector(0, 1, 0)  },
		{ pos = Vector(0, 0, 0), u = 0, v = 1, normal = Vector(0, -1, 0) }, { pos = Vector(0, 0, z), u = 0, v = 0, normal = Vector(0, -1, 0) }, { pos = Vector(x, 0, 0), u = 1, v = 1, normal = Vector(0, -1, 0) },
		{ pos = Vector(0, 0, z), u = 0, v = 0, normal = Vector(0, -1, 0) }, { pos = Vector(x, 0, z), u = 1, v = 0, normal = Vector(0, -1, 0) }, { pos = Vector(x, 0, 0), u = 1, v = 1, normal = Vector(0, -1, 0) },
		{ pos = Vector(0, 0, 0), u = 0, v = 1, normal = Vector(-1, 0, 0) }, { pos = Vector(0, y, 0), u = 1, v = 1, normal = Vector(-1, 0, 0) }, { pos = Vector(0, 0, z), u = 0, v = 0, normal = Vector(-1, 0, 0) },
		{ pos = Vector(0, 0, z), u = 0, v = 0, normal = Vector(-1, 0, 0) }, { pos = Vector(0, y, 0), u = 1, v = 1, normal = Vector(-1, 0, 0) }, { pos = Vector(0, y, z), u = 1, v = 0, normal = Vector(-1, 0, 0) },
		{ pos = Vector(x, 0, 0), u = 0, v = 1, normal = Vector(1, 0, 0)  }, { pos = Vector(x, 0, z), u = 0, v = 0, normal = Vector(1, 0, 0)  }, { pos = Vector(x, y, 0), u = 1, v = 1, normal = Vector(1, 0, 0)  },
		{ pos = Vector(x, 0, z), u = 0, v = 0, normal = Vector(1, 0, 0)  }, { pos = Vector(x, y, z), u = 1, v = 0, normal = Vector(1, 0, 0)  }, { pos = Vector(x, y, 0), u = 1, v = 1, normal = Vector(1, 0, 0)  },
	}
	
	net.receive("ent", function()
		net.readEntity(function(ent)
			local ent_mesh = mesh.createFromTable(vertices)
			local ent_mat = material.create("VertexLitGeneric")
			ent_mat:setTexture("$basetexture", "phoenix_storms/stripes")
			
			ent:setMesh(ent_mesh)
			ent:setMeshMaterial(ent_mat)
		end)
	end)
end
