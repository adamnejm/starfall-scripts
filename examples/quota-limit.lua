--@name Quota Limit Example
--@author Name
--@shared

local owner, client, chip, world = owner(), player(), chip(), entity(0)

-------------------------------------------

local server_limit = 0.1
local client_limit = 0.2

-------------------------------------------

if client ~= owner then return end

local limit = SERVER and server_limit or client_limit
local routine = coroutine.wrap(function()
	while true do
		if cpuAverage() > cpuMax() * limit then
			
			-- Process stuff...
			
			coroutine.yield()
		end
	end
end)

hook.add("Tick", "", routine)
