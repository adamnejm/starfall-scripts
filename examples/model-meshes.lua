--@name Model Meshes Example
--@author Name
--@shared

local owner, client, chip, world = owner(), player(), chip(), entity(0)

-------------------------------------------

local scale   = Vector(2 * 6)
local offset  = Vector(256, 400)
local model   = "models/maxofs2d/companion_doll.mdl"
local texture = "maxofs2d/models/companion_doll"

-------------------------------------------

if SERVER then
	local screen = prop.createComponent(chip:getPos() + chip:getUp() * 50, Angle(90, 0, 0), "starfall_screen", "models/hunter/plates/plate2x2.mdl", true)
	screen:linkComponent(chip)
else
	local vertices = mesh.getModelMeshes(model, 0)[1].triangles
	-- `mesh.createFromTable` will panic if it encounters unknown fields, so clear all `weights`
	for _, v in pairs(vertices) do
		v.weights = nil
	end
	
	local render_mesh = mesh.createFromTable(vertices)
	local render_material = material.load(texture)

	local transform = Matrix()
	transform:setScale(scale)
	transform:setTranslation(offset)
	
	hook.add("Render", "", function()
		transform:setAngles(Angle(timer.curtime() * 100, 0, 90))
		render.pushMatrix(transform)
			render.setColor(Color(255, 255, 255))
			render.setMaterial(render_material)
			render_mesh:draw()
		render.popMatrix()
	end)
end
