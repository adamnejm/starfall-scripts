--@name Wobble Example
--@author Name
--@server

local owner, client, chip, world = owner(), player(), chip(), entity(0)

-------------------------------------------

local force = 0.93
local damping = 15

-------------------------------------------

local holo = hologram.create(chip:getPos() + chip:getUp() * 20, Angle(), "models/holograms/cube.mdl")
local previous_pos = chip:getPos()
local target_dir = Vector()

hook.add("Think", "", function()
	local delta_pos = holo:getPos() - previous_pos
	previous_pos = chip:getPos()
	
	target_dir = (target_dir - delta_pos / damping) * force
	holo:setPos(holo:getPos() + target_dir)
end)
