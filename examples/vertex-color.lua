--@name Vertex Color Example
--@author Name
--@shared

local owner, client, chip, world = owner(), player(), chip(), entity(0)

-------------------------------------------

if SERVER then
	local screen = prop.createComponent(chip:getPos() + chip:getUp() * 50, Angle(90, 0, 0), "starfall_screen", "models/hunter/plates/plate2x2.mdl", true)
	screen:linkComponent(chip)
else
	local function rainbow(offset)
		local t = (offset + timer.curtime() * 100) % 360
		return Color(t, 1, 1):hsvToRGB()
	end
	
	local vertices = {
		{ pos = Vector(0, 0) }, { pos = Vector(512, 512) }, { pos = Vector(0, 512)   },
		{ pos = Vector(0, 0) }, { pos = Vector(512, 0)   }, { pos = Vector(512, 512) },
	}
	
	hook.add("Render", "", function()
		vertices[1].color = rainbow(0)
		vertices[2].color = rainbow(180)
		vertices[3].color = rainbow(90)
		vertices[4].color = rainbow(0)
		vertices[5].color = rainbow(270)
		vertices[6].color = rainbow(180)
		local render_mesh = mesh.createFromTable(vertices)
		
		render.setMaterial()
		render.setColor(Color(255, 255, 255))
		render_mesh:draw()
		render_mesh:destroy()
	end)
end
