--@name Rounded Poly Example
--@author Name
--@client

local owner, client, chip, world = owner(), player(), chip(), entity(0)

-------------------------------------------

local pi, cos, sin = math.pi, math.cos, math.sin
local function getPoly(x, y, w, h, r, f)
	local poly = {}
	local f2 = f*2
	
	for i = f * 0, f * 1 do local rad = i / f2 * pi; poly[#poly + 1] = { x = x + r     - cos(rad) * r, y = y + r     - sin(rad) * r } end
	for i = f * 1, f * 2 do local rad = i / f2 * pi; poly[#poly + 1] = { x = x + w - r - cos(rad) * r, y = y + r     - sin(rad) * r } end
	for i = f * 2, f * 3 do local rad = i / f2 * pi; poly[#poly + 1] = { x = x + w - r - cos(rad) * r, y = y + h - r - sin(rad) * r } end
	for i = f * 3, f * 4 do local rad = i / f2 * pi; poly[#poly + 1] = { x = x + r     - cos(rad) * r, y = y + h - r - sin(rad) * r } end
	
	return poly
end

-------------------------------------------

local rounded_poly = getPoly(256, 256, 512, 128, 32, 8)

hook.add("DrawHUD", "", function()
	render.setMaterial()
	render.setColor(Color(255, 255, 255))
	render.drawPoly(rounded_poly)
end)

if client == owner then
	enableHud(nil, true)
end
