--@name Antenna
--@author Name
--@server

local owner, client, chip, world = owner(), player(), chip(), entity(0)

-------------------------------------------

local count = 7
local length = 3
local thickness = 0.6

-------------------------------------------

local holos = { [0] = chip }
for i = 1, count do
	local segment = hologram.create(chip:getPos() + chip:getUp() * (length * (i - 1) + length / 2), chip:getAngles(), "models/holograms/hq_cylinder.mdl", Vector(thickness, thickness, length) / 12)
	segment:setParent(holos[i - 1])
	
	local ball = hologram.create(segment:localToWorld(Vector(0, 0, length / 2)), chip:getAngles(), "models/holograms/hq_sphere.mdl", Vector(thickness) / 12)
	ball:setParent(segment)
	
	holos[i] = ball
end

local vel_global_old = Vector()
hook.add("Tick", "", function()
	local vel_global = chip:getPos()
	local vel = chip:worldToLocalVector(vel_global_old - vel_global) * 5
	vel_global_old = vel_global
	
	local ray = trace.line(owner:getShootPos(), owner:getShootPos() + owner:getAimVector() * 1000, { owner, chip })
	local pos = ray.HitPos + ray.HitNormal * 50
	local dist = chip:getPos():getDistance(pos) / 10
	chip:getPhysicsObject():setPos(chip:getPos() + (pos - chip:getPos()):getNormalized() * dist)
	chip:getPhysicsObject():wake()
	
	for i, holo in ipairs(holos) do
		local prev = holos[i - 1]
		local ang = Angle(math.clamp(vel.x, -10, 10), 0, math.clamp(-vel.y, -10, 10))
		holo:setAngles(prev:localToWorldAngles(ang))
	end
end)
